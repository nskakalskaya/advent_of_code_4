#include <iostream>
#include <string>
#include <algorithm>

constexpr uint32_t begin_range{256310};
constexpr uint32_t end_range{732736};

const bool IsMeetCriteria(std::uint32_t number)
{
    std::int64_t last_digit{-1};
    auto is_only_increasing{true};
    auto is_found_doubles{false};
    while (number > 0U)
    {
        if (last_digit == -1)
        {
            last_digit = number % 10;
            number /= 10;
        }
        else
        {
            if (last_digit < number % 10)
            {
                is_only_increasing = false;
                break;
            }
            else if (last_digit == number % 10)
            {
                is_found_doubles = true;
            }

            last_digit = number % 10;
            number /= 10;
        }
    }

    return is_only_increasing && is_found_doubles;
}


const uint32_t FindNumberOfPasswords()
{
    std::uint32_t count_passwords{0U};

    for (auto number{begin_range}; number<= end_range; ++number)
    {
        if (IsMeetCriteria(number))
        {
            count_passwords++;
        }
    }
    return count_passwords;
}


int main()
{
    try
    {
        /* https://adventofcode.com/2019/day/4
         * The goal is to find the amount of numbers that lay in range
         * between begin_range and end_range and
         * have no decrease in digits from left to right
         * and have one or more adjecent doubles like 11113, 234558, etc.
         * The answer is 979.
        */
        std::cout << FindNumberOfPasswords() << std::endl;
    }
    catch (const std::exception &e)
    {
        std::cout << "Caught exception " << e.what() << std::endl;
        return -1;
    }
    return 0;
}
